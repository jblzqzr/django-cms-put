# Django CMS Put

Repositorio de plantilla para el ejercicio "Django CMS_Put". Recuerda que puedes consultar su enunciado en la guía de estudio (programa) de la asignatura.

Metodos PERMITIDOS:

    - GET /: Muestra todos los recursos disponibles
    - PUT /: Permite crear un nuevo recurso, si la petición sigue el modelo: request=""&content=""
    - GET /{recurso}: Muestra el contenido del recurso de la base de datos
    - PUT /{recurso}: Permite actualizar el contenido del recurso de la base de datos, si la petición sigue el modelo: content=""

El usuario para acceder a las funciones de admin es:
    
    - Usuario: admin
    - Contraseña: admin
