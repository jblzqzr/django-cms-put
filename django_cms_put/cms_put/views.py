from django.http import HttpResponse
from .models import Content
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

INDEX_PAGE = """<!DOCTYPE html>
<html lang="es">
<body>
    <h2>Lista de Recursos</h2>
    <ul>
        {lista}
    </ul>
</body>
</html>"""


@csrf_exempt
def index(request):
    if request.method == "PUT":
        try:
            put_body = request.body.decode("utf-8").split("&")
            if len(put_body) > 2:
                return HttpResponse("Solo una petición por PUT")
            else:
                id_recurso = put_body[0].split("=")[0]
                recurso = put_body[0].split("=")[1]
                id_content = put_body[1].split("=")[0]
                content = put_body[1].split("=")[1]
                if id_recurso == 'request' and id_content == 'content':
                    try:
                        c = Content.objects.get(request=recurso)
                        # Si existe, lo actualizo.
                        c.content = content

                    except Content.DoesNotExist:
                        c = Content(request=recurso, content=content)
                    c.save()

                else:
                    raise Exception()

        except IndexError:
            return HttpResponse("Error en la petición")

    valores_columna = Content.objects.values_list('request', flat=True)
    # Convertir el queryset en una lista de valores
    lista_valores = list(valores_columna)
    lista = ""
    for request in lista_valores:
        lista += "<li><a href='/{recurso}'>{recurso}</a></li>"
        lista = lista.format(recurso=request)
    return HttpResponse(INDEX_PAGE.format(lista=lista))


@csrf_exempt
def contents(request, llave):
    if request.method == "GET":
        print(request.path)
        try:
            contenido = Content.objects.get(request=llave)
        except Content.DoesNotExist:
            return HttpResponse("Recurso no encontrado")
        return HttpResponse(contenido.content)
    elif request.method == "PUT":
        try:
            put_body = request.body.decode("utf-8").split("&")
            if len(put_body) > 1:
                return HttpResponse("Sobran campos en la peticion")
            else:
                recurso = request.path.split("/", 2)[1]
                print(recurso)
                id_content = put_body[0].split("=")[0]
                content = put_body[0].split("=")[1]
                if id_content == 'content':
                    c = Content.objects.get(request=recurso)
                    # LO actualizo.
                    c.content = content
                    c.save()

                else:
                    raise Exception()
            return HttpResponse("Recurso actualizado")
        except (IndexError, Exception):
            return HttpResponse("Error en la petición")
